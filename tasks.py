from invoke import task
from dummy.celery_tasks.run_task import run
from subprocess import call, Popen
from multiprocessing import Process


@task
def install_postgres(ctx):

    '''Install and configure postgres. Currently working awfully.'''

    ctx.run("sudo touch /etc/apt/sources.list.d/pgdg.list")

    ctx.run('wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF'
            '8.asc | sudo apt-key add -')

    ctx.run('sudo apt-get update')

    ctx.run('sudo apt-get install postgresql-9.2')

    ctx.run('sudo -u postgres psql -c "ALTER USER postgres WITH '
            'PASSWORD \'123456\';"')

    ctx.run('sudo -u postgres psql -c "create user django with '
            'password \'djangopass\';"')

    ctx.run('sudo -u postgres psql -c "alter role django set client_encoding '
            'to \'utf8\';"')

    ctx.run('sudo -u postgres psql -c "alter role django '
            'set default_transaction_isolation to \'read committed\';"')

    ctx.run('sudo -u postgres psql -c "alter role django set'
            ' timezone to \'UTC\';"')

    ctx.run('sudo -u postgres psql -c "create database django_db owner'
            ' django;"')


@task
def make_migrations(ctx):

    '''Process database migrations'''

    ctx.run("python3 manage.py makemigrations dummy")
    ctx.run("python3 manage.py migrate")


@task
def run_server(ctx):

    '''Run debug server'''

    call("python3 manage.py runserver 0:8080")


@task
def run_postgres(ctx):

    '''Start postgres server'''

    ctx.run("sudo service postgresql start")


@task
def install_rabbit(ctx):

    '''Install and configure rabbitmq'''

    ctx.run('wget -O- https://www.rabbitmq.com/rabbitmq-release-signing-key'
            '.asc | sudo apt-key add -')

    ctx.run('sudo apt-get update')
    ctx.run('sudo apt-get install rabbitmq-server')


@task
def run_rabbit(ctx):

    '''Start rabbitmq'''

    ctx.run('sudo service rabbitmq-server start')


@task
def run_workers(ctx):

    '''Run celery workers'''

    p = Process(target=run, args=())
    p.start()


@task
def run_checks(ctx):

    '''Make codestyle checks'''

    ctx.run('flake8')
    # ctx.run('pylint3 --output-format colorized dummy')


@task
def run_flower(ctx):
    
    '''run celery monitoring'''
    
    ctx.run('flower --port=8081')

from django.db import models
from django.db.models.fields import (DateTimeField,
                                     TextField)


class DataSet(models.Model):
    data = TextField()

    class Meta:
        db_table = "Dataset"


class Result(models.Model):
    date = DateTimeField(auto_now=True)
    dataset = models.ForeignKey(DataSet)
    result = TextField()
    exception = TextField()
    # Result.objects.latest('date')

    class Meta:
        db_table = "Result"

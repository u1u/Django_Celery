from django.conf.urls import url
from dummy.views import index, proceed, getdata, show_results


urlpatterns = [
    url('^$', index),
    url('pst', getdata),
    url('proceed', proceed),
    url('results', show_results),
]

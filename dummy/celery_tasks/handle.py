'''
#To run on a separate machine:

import os, sys
import django
sys.path.append(os.path.dirname(os.path.abspath(__file__))[:-19])
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dummy.settings")
django.setup()
'''

# from time import sleep
import json
from celery import Celery
from celery.bin import worker

handle_app = Celery(backend="amqp://",
                    broker="amqp://",
                    loglevel='INFO',
                    traceback=True)

handle_app.conf.update(CELERY_ACCEPT_CONTENT=['json'],
                       CELERY_TASK_SERIALIZER='json',
                       CELERY_RESULT_SERIALIZER='json')


@handle_app.task(name="handle", bind=True)
def handle(self, data, set_id):
    mas = json.loads(data)['data']
    res = dict({
        'set_id': set_id,
        'data': [],
        'error': 'none'
    })
    for item in mas:
        # sleep(0.5)  # some delay
        res['data'].append(int(item['A'])+int(item['B']))

    ret_string = json.dumps(res)

    return (ret_string)


handle_options = {
    'backend': "amqp://",
    'broker': "amqp://",
    'loglevel': 'INFO',
    'traceback': True,
}

handle_worker = worker.worker(app=handle_app)

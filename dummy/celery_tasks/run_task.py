import os
import sys
from multiprocessing import Process
import django

sys.path.append(os.path.dirname(os.path.abspath(__file__))[:-19])
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dummy.settings")
django.setup()

from dummy.celery_tasks.dbwrite import dbwrite_worker, dbwrite_options  # noqa E402
from dummy.celery_tasks.dbread import dbread_worker, dbread_options  # noqa E402
from dummy.celery_tasks.handle import handle_worker, handle_options  # noqa E402


def run():
    p1 = Process(target=dbwrite_worker.run, args=('dbwrite@prime',),
                 kwargs=dbwrite_options)
    p2 = Process(target=dbread_worker.run, args=('dbread@prime',),
                 kwargs=dbread_options)
    p3 = Process(target=handle_worker.run, args=('handle@prime',),
                 kwargs=handle_options)

    p1.start()
    p2.start()
    p3.start()
    return "OK"


if __name__ == "__main__":
    run()

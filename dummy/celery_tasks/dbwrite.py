'''
# To run on a separate machine:
import os, sys
import django
sys.path.append(os.path.dirname(os.path.abspath(__file__))[:-19])
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dummy.settings")
django.setup()
'''

from json import loads, dumps
from celery import Celery
from celery.bin import worker
from dummy.models import DataSet, Result

write_app = Celery(backend="amqp://",
                   broker="amqp://",
                   loglevel='INFO',
                   traceback=True)

write_app.conf.update(CELERY_ACCEPT_CONTENT=['json'],
                      CELERY_TASK_SERIALIZER='json',
                      CELERY_RESULT_SERIALIZER='json')


@write_app.task(name="task_that_does_nothing")
def nothing(prev):
    return "OK"


@write_app.task(name="db_write_dataset", bind=True)
def db_write_dataset(self, data):
    dataset = DataSet(data=data)
    dataset.save()
    return ("OK")


@write_app.task(bind=True, name="db_write_result")
def db_write_result(self, data):
    dct = loads(data)
    res = ""
    if dct['error'] == 'none':
        ds_unit = DataSet.objects.get(pk=dct['set_id'])
        res = Result(dataset=ds_unit, result=dumps(dct['data']),
                     exception=dct['error'])
    else:
        res = Result(dataset_id=1, result="",
                     exception=dct['error'])
    res.save()
    return "OK"


dbwrite_options = {
    'backend': "amqp://",
    'broker': "amqp://",
    'loglevel': 'INFO',
    'traceback': True,
}

dbwrite_worker = worker.worker(app=write_app)

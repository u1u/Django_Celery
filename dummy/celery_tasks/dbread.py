'''
#To run on a separate machine:

import os, sys
import django
sys.path.append(os.path.dirname(os.path.abspath(__file__))[:-19])
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dummy.settings")
django.setup()
'''

from celery import Celery
from celery.bin import worker
from dummy.models import DataSet, Result

read_app = Celery(backend="amqp://",
                  broker="amqp://",
                  loglevel='INFO',
                  traceback=True)

read_app.conf.update(CELERY_ACCEPT_CONTENT=['json'],
                     CELERY_TASK_SERIALIZER='json',
                     CELERY_RESULT_SERIALIZER='json')


@read_app.task(name="db_read_dataset", bind=True)
def db_read_ds(self, set_id):
    dataset = DataSet.objects.get(pk=set_id)
    return dataset.data


@read_app.task(name="db_read_last_results", bind=True)
def db_read_result(self):
    ids = DataSet.objects.filter().values_list('id', flat=True)
    res_dct = {
        'data': []
    }
    for ds_id in ids:
        res_obj = Result.objects.filter(dataset_id=ds_id).latest('date')
        res_obj_jsonable = {
            'dataset_id': res_obj.dataset_id,
            'result': res_obj.result,
            'exception': res_obj.exception
        }
        res_dct['data'].append(res_obj_jsonable)
    return res_dct


dbread_options = {
    'backend': "amqp://",
    'broker': "amqp://",
    'loglevel': 'INFO',
    'traceback': True,
}

dbread_worker = worker.worker(app=read_app)

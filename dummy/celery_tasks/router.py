from copy import deepcopy
from celery import chain, group
from dummy.celery_tasks.dbwrite import db_write_result, nothing
from dummy.celery_tasks.dbread import db_read_ds
from dummy.celery_tasks.handle import handle
from dummy.models import DataSet


def run_processing(res_dict):

    '''processing all sets of data one by one'''

    ids = DataSet.objects.filter().values_list('id', flat=True)
    for set_id in ids:

        # read from db
        read_call = db_read_ds.delay(set_id)
        data = read_call.get()

        # handle data
        handle_call = handle.delay(data, set_id)

        # get results and write them to shared dict
        res_from_handler = handle_call.get()
        res_dict[set_id] = res_from_handler

        # write results to db
        write_call = db_write_result.delay(res_from_handler)
        write_call.get()

    res_dict['status'] = 'READY'


def run_processing_chains(res_dict):

    '''Processing all sets of data in parallel chains'''

    ids = DataSet.objects.filter().values_list('id', flat=True)
    chains = []
    if len(ids) == 0:
        res_dict['status'] = 'ERROR'
        res_dict['content'] = 'Nothing to process'
        db_write_result.delay(deepcopy(res_dict))
        return

    chains.append(chain(nothing.s('s'), nothing.s()))
    for set_id in ids:
        chains.append(chain(db_read_ds.s(set_id),
                            handle.s(set_id),
                            db_write_result.s()))

    chain_group = group(*chains)
    group_res = chain_group.apply_async()
    group_res.get()
    res_dict['status'] = 'READY'

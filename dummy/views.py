import json
from multiprocessing import Process, Manager
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from django.http import HttpResponse
from django.template import loader
from dummy.celery_tasks.dbwrite import db_write_dataset
from dummy.celery_tasks.router import run_processing_chains
from dummy.celery_tasks.dbread import db_read_result

schema = {
    "properties": {
        "data": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "A": {
                        "type": "string",
                        "pattern": "^[-+]?[0-9]+$"
                    },
                    "B": {
                        "type": "string",
                        "pattern": "^[-+]?[0-9]+$"
                    }
                },
                "required": ["A", "B"]
            }
        }
    },
    "required": ["data"]
}

manager = Manager()
shared_res = manager.dict()
shared_res['status'] = 'IDLE'

# 400 - bad request, 409 - conflict, 500 - internal server error


def index(request):

    '''Get the page with form'''

    if request.method == 'GET':
        tmpl = loader.get_template("index.html")
        return HttpResponse(tmpl.render())


def getdata(request):

    '''Run celery task that writes dataset to db'''

    if request.method == 'POST':
        req_text = request.body.decode('utf-8')
        req_json = json.loads(request.body.decode('utf-8'))
        response = HttpResponse()
        try:
            validate(req_json, schema)
            db_write_dataset.delay(req_text)
            response.content = 'OK'

        except ValidationError:
            response.status_code = 400
            response.reason = 'Not valid JSON'

        return response


def proceed(request):

    '''Run celery task that makes calculations'''
    response = HttpResponse()
    if request.method == 'GET':
        if shared_res['status'] != 'PROGRESS':
            global processing
            shared_res['status'] = 'PROGRESS'
            processing = Process(target=run_processing_chains,
                                 args=(shared_res,))
            processing.start()
            response.status_code = 200
        else:
            response.status_code = 409
            response.reason = 'Calculating is in progress'

        return response


def show_results(request):

    '''Get JSON with results of last calculation'''

    response = HttpResponse()
    if request.method == 'GET':
        if shared_res['status'] == 'PROGRESS':
            response.status_code = 409
            response.reason = 'Calculating is in progress'
        else:
            shared_res['status'] = 'IDLE'

            # TODO: make this async
            res_dct = db_read_result.delay().get()
            if len(res_dct['data']) == 0:
                response.status_code = 500
                response.reason = 'No calculations were made'
            else:
                response.status_code = 200
                response.content = json.dumps(res_dct)
    return response
